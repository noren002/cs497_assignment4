#!flask/bin/python
from flask import Flask, request, abort
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import numpy
from datetime import date

app = Flask(__name__)


@app.route('/data/sample', methods=['GET'])
def get():
    server_info = str(
        "Created by: Jacob Norenberg, Ryan Carrol, and Gregory Givens\nServer: Diabetes Inference Research\nDate "
        "Accessed: " + str(
            date.today()) + "\n\n")
    return server_info


@app.route('/predict/sample', methods=['POST'])
def predict():
    if not request.json:
        abort(400)

    sample = {
        'pregnant': request.json['pregnant'],
        'glucose': request.json['glucose'],
        'bp': request.json['bp'],
        'skin': request.json['skin'],
        'insulin': request.json['insulin'],
        'bmi': request.json['bmi'],
        'pedigree': request.json['pedigree'],
        'age': request.json['age']
    }
    data = [sample['pregnant'], sample['glucose'], sample['bp'], sample['skin'], sample['insulin'], sample['bmi'],
            sample['pedigree'], sample['age']]
    np_data = numpy.array(data)
    np_data = np_data.reshape(1, -1)
    col_names = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age', 'label']
    url = "https://gist.githubusercontent.com/ktisha/c21e73a1bd1700294ef790c56c8aec1f/raw" \
          "/819b69b5736821ccee93d05b51de0510bea00294/pima-indians-diabetes.csv "
    pima = pd.read_csv(url, header=None, names=col_names, skiprows=9)
    feature_cols = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age']
    X = pima[feature_cols]
    y = pima.label
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)
    logreg = LogisticRegression()
    logreg.fit(X_train, y_train)
    result = logreg.predict(np_data)
    # result = logreg.score(X_test, y_test)
    if result == 0:
        return str("Your input predicted that you will not have diabetes.\n\n")
    else:
        return str("Your input predicted that you will have diabetes.\n\n")


if __name__ == '__main__':
    app.run(debug=True)
